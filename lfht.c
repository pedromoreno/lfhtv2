#include <stdlib.h>
#include <stdint.h>
#include <stdatomic.h>
#include <string.h>
#include "lfht.h"

union lfht_node;

struct hash_node {
	union lfht_node *_Atomic array[0];
};

struct leaf_node {
	size_t hash;
	void *key;
	void *value;
};

struct leaf_bucket {
	int size;
	struct leaf_node array[0];
};

union lfht_node {
	struct hash_node hash;
	struct leaf_bucket bucket;
};


int search_remove(union lfht_node *hnode, size_t hash, void **value);

int search_insert(union lfht_node *hnode, size_t hash, void *key, void **value);

int search_node(union lfht_node *hnode, size_t hash, void **value);

union lfht_node *create_hash_node(int size);

//interface

struct lfht_head *init_lfht(int max_threads)
{
	struct lfht_head *head = malloc(sizeof(struct lfht_head));
	head->entry_hash = create_hash_node(HASH_SIZE);
	head->max_threads = max_threads;
	return head;
}

void *lfht_search(struct lfht_head *head, size_t hash)
{
	void *value = NULL;
	search_node(head->entry_hash, hash, &value);
	return value;
}

void *lfht_insert(struct lfht_head *head, size_t hash, void *value)
{
	search_insert(head->entry_hash, hash, NULL, &value);
	return value;
}

void *lfht_remove(struct lfht_head *head, size_t hash)
{
	void *value = NULL;
	search_remove(head->entry_hash, hash, &value);
	return value;
}

//auxiliary

union lfht_node *add_leaf_node(size_t hash,
                               void *key,
                               void *value,
                               union lfht_node *prev)
{
	int size;
	if(prev)
		size = prev->bucket.size;
	else
		size = 0;
	union lfht_node *node = malloc(sizeof(struct leaf_bucket) +
	                               (sizeof(struct leaf_node) * (size + 1)));
	if(prev)
		memcpy(&(node->bucket.array[0]),
		       &(prev->bucket.array[0]),
		       sizeof(struct leaf_node) * (size));
	node->bucket.array[size].hash = hash;
	node->bucket.array[size].key = key;
	node->bucket.array[size].value = value;
	node->bucket.size = size + 1;
	return node;
}

union lfht_node *rm_leaf_node(int index, union lfht_node *prev)
{
	if(prev->bucket.size == 1)
		return NULL;
	union lfht_node *node = malloc(
			sizeof(struct leaf_bucket) +
			(sizeof(struct leaf_node) * (prev->bucket.size - 1)));
	memcpy(&(node->bucket.array[0]),
	       &(prev->bucket.array[0]),
	       sizeof(struct leaf_node) * index);
	memcpy(&(node->bucket.array[index]),
	       &(prev->bucket.array[index + 1]),
	       sizeof(struct leaf_node) * ((prev->bucket.size - index) - 1));
	node->bucket.size = prev->bucket.size - 1;
	return node;
}

union lfht_node *create_hash_node(int size)
{
	union lfht_node *node = malloc((1 << size) * sizeof(union lfht_node *));
	for(int i = 0; i < 1 << size; i++) {
		atomic_init(&(node->hash.array[i]), NULL);
	}
	return node;
}

int get_bucket(size_t hash, int level, int size)
{
	return (hash >> (64 - ((level + 1) * size))) & ((1 << size) - 1);
}

union lfht_node *valid_ptr(union lfht_node *next)
{
	return (union lfht_node *)((uintptr_t)next & ~1);
}

unsigned get_flag(union lfht_node *ptr)
{
	return (uintptr_t)ptr & 1;
}

union lfht_node *set_flag(union lfht_node *next)
{
	return (union lfht_node *)((uintptr_t)next | 1);
}

int find_node(size_t hash,
              int *level,
              union lfht_node **hnode,
              union lfht_node **nodeptr,
              int *index)
{
	int pos = get_bucket(hash, *level, HASH_SIZE);
	union lfht_node *iter = atomic_load_explicit(
			&((*hnode)->hash.array[pos]), memory_order_consume);
	if(get_flag(iter)) {
		*hnode = valid_ptr(iter);
		(*level)++;
		return find_node(hash, level, hnode, nodeptr, index);
	}
	*nodeptr = iter;
	if(!iter) {
		return 0;
	}
	for(int i = 0; i < iter->bucket.size; i++) {
		if(iter->bucket.array[i].hash == hash) {
			//TODO key_cmp
			*index = i;
			return 1;
		}
	}
	return 0;
}

int search_remove(union lfht_node *hnode, size_t hash, void **value)
{
	union lfht_node *cnode;
	int level = 0, index;
	while(find_node(hash, &level, &hnode, &cnode, &index)) {
		union lfht_node *new = rm_leaf_node(index, cnode);
		if(atomic_compare_exchange_strong_explicit(
				   &(hnode->hash.array[get_bucket(
						   hash, level, HASH_SIZE)]),
				   &cnode,
				   new,
				   memory_order_release,
				   memory_order_relaxed)) {
			*value = cnode->bucket.array[index].value;
			//retire(cnode);
			return 1;
		} else {
			free(new);
		}
	}
	return 0;
}

int search_insert(union lfht_node *hnode, size_t hash, void *key, void **value)
{
	union lfht_node *cnode;
	int level = 0, index;
	while(!find_node(hash, &level, &hnode, &cnode, &index)) {
		if(cnode && cnode->bucket.size >= MAX_NODES) {
			union lfht_node *new_hash = create_hash_node(HASH_SIZE);
			for(int i = 0; i < cnode->bucket.size; i++) {
				int bucket = get_bucket(
						cnode->bucket.array[i].hash,
						level + 1,
						HASH_SIZE);
				union lfht_node *prev = atomic_load_explicit(
						&(new_hash->hash.array[bucket]),
						memory_order_relaxed);
				atomic_init(&(new_hash->hash.array[bucket]),
				            add_leaf_node(cnode->bucket.array[i].hash,
				                          cnode->bucket.array[i].key,
				                          cnode->bucket.array[i].value,
				                          prev));
				free(prev);
			}
			if(atomic_compare_exchange_strong_explicit(
					   &(hnode->hash.array[get_bucket(
							   hash, level, HASH_SIZE)]),
					   &cnode,
					   set_flag(new_hash),
					   memory_order_release,
					   memory_order_relaxed)) {
				//retire(cnode);
			} else {
				for(int i = 0; i < HASH_SIZE; i++) {
					free(new_hash->hash.array[i]);
				}
				free(new_hash);
			}
		} else {
			union lfht_node *new_node = add_leaf_node(
					hash, key, *value, cnode);
			if(atomic_compare_exchange_strong_explicit(
					   &(hnode->hash.array[get_bucket(
							   hash, level, HASH_SIZE)]),
					   &cnode,
					   new_node,
					   memory_order_release,
					   memory_order_relaxed)) {
				//retire(cnode);
				return 1;
			} else {
				free(new_node);
			}
		}
	}
	*value = cnode->bucket.array[index].value;
	return 0;
}

int search_node(union lfht_node *hnode, size_t hash, void **value)
{
	union lfht_node *cnode;
	int level = 0, index;
	if(find_node(hash, &level, &hnode, &cnode, &index)) {
		*value = cnode->bucket.array[index].value;
		return 1;
	} else {
		return 0;
	}
}


#ifndef __LFHT_H__
#define __LFHT_H__

#include <stddef.h>

#ifndef LFHT_DEBUG
#define LFHT_DEBUG 0
#endif

#if LFHT_DEBUG

#define MAX_NODES 2
#define HASH_SIZE 1

#else

#define MAX_NODES 5
#define HASH_SIZE 4

#endif

struct lfht_head {
	union lfht_node *entry_hash;
	int max_threads;
};


struct lfht_head *init_lfht(int max_threads);

void *lfht_search(struct lfht_head *head, size_t hash);

void *lfht_insert(struct lfht_head *head, size_t hash, void *value);

void *lfht_remove(struct lfht_head *head, size_t hash);

//debug interface


void *lfht_debug_search(struct lfht_head *head, size_t hash);

#endif // __LFHT_H__

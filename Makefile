CC=gcc
CFLAGS=-Wall -fPIC
AR=ar
OPT=-O3
LFLAGS=-shared
DEBUG=-Og -DLFHT_DEBUG=1
SYM=-g -ggdb

.PHONY: default debug all clean

default: liblfht.a
debug: liblfht_debug.a liblfht_debug.so
sym: liblfht_sym.a liblfht_sym.so
all: liblfht.a liblfht.so

liblfht.so: lfht.o
	$(CC) lfht.o $(CFLAGS) $(INCLUDE) $(OPT) $(LFLAGS) -o liblfht.so

liblfht.a: lfht.o
	$(AR) rcu liblfht.a lfht.o

lfht.o: lfht.c lfht.h
	$(CC) -c lfht.c $(CFLAGS) $(INCLUDE) $(OPT) $(LFLAGS)

liblfht_debug.so: lfht_debug.o
	$(CC) lfht_debug.o $(CFLAGS) $(INCLUDE) $(DEBUG) $(SYM) $(LFLAGS) -o liblfht_debug.so

liblfht_debug.a: lfht_debug.o
	$(AR) rcu liblfht_debug.a lfht_debug.o

lfht_debug.o: lfht.c lfht.h
	$(CC) -c lfht.c $(CFLAGS) $(INCLUDE) $(DEBUG) $(SYM) $(LFLAGS) -o lfht_debug.o

liblfht_sym.so: lfht_sym.o
	$(CC) lfht_sym.o $(CFLAGS) $(INCLUDE) $(OPT) $(SYM) $(LFLAGS) -o liblfht_sym.so

liblfht_sym.a: lfht_sym.o
	$(AR) rcu liblfht_sym.a lfht_sym.o

lfht_sym.o: lfht.c lfht.h
	$(CC) -c lfht.c $(CFLAGS) $(INCLUDE) $(OPT) $(SYM) $(LFLAGS) -o lfht_sym.o

clean:
	rm -f *.o *.a *.so
